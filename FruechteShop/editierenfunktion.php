<?php

if (!isset($_POST["id"]))
    die ("keine id übergeben");
if (!isset($_POST["beschreibung"]))
    die ("keine beschreibung übergeben");
if (!isset($_POST["kosten"]))
    die ("keine kosten übergeben");



include "db.php";

$sql = $db -> prepare('UPDATE Artikel SET 
                        a_name = :name,  
                        a_beschreibung = :beschreibung, 
                        a_kosten = :kosten, 
                        a_anz = :anzahl, 
                        l_id = :lager
                        WHERE a_id = :id');


    $sql -> bindParam(':id',$_POST['id'], PDO::PARAM_INT);
    $sql -> bindParam(':name',$_POST['name'], PDO::PARAM_STR);
    $sql -> bindParam(':beschreibung',$_POST['beschreibung'], PDO::PARAM_STR);
    $sql -> bindParam(':kosten',$_POST['kosten'], PDO::PARAM_INT);
    $sql -> bindParam(':anzahl',$_POST['anzahl'], PDO::PARAM_INT);
    $sql -> bindParam(':lager',$_POST['lager'], PDO::PARAM_INT);
    $sql -> execute();


header("Location: mainpage_admin.php");

?>