
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Früchte Shop</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/logo-nav.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="mainpage_client.php">FrüchteShop</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php
                include 'db.php';
                
    
                
                $res = $db->query ("SELECT * from Artikel where a_id =". $_GET['id']);
                $tmp = $res->fetchAll(PDO::FETCH_ASSOC);

                foreach ($tmp as $row) :?>
    <div>
	<p>Produkt: <?php echo $row['a_name'];?> </p>
    <p>Beschreibung: <?php echo $row['a_beschreibung'];?> </p>
    <p id="ges" cost=<?php echo $row['a_kosten'];?>>Kosten pro Stück: <?php echo $row['a_kosten'];?> </p>
    <p>Anzahl im Lager: <?php echo $row['a_anz'];?> </p>
    
    <input type="number" class="anzahl" id="anz"></input>
    
    <input type="submit" value="Kaufen" class="bestätigen"  ></input>
	</p></div>
                <?php endforeach; ?>
    

    
            </div>
        </div>
    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
<script>
    
            $('.bestätigen').click(function(){
                    var anz = $('#anz').val();
    				var id = $(this).attr('');
                    
                    var ges = $('#ges').attr('cost')*anz;
                    
                    window.location.href="mainpage_client.php";
                    
                    
    				$.ajax({
    					url:"http://127.0.0.1/FruechteShop/kauffunktion.php",
                        data:"anz=" + anz +"&ges=" + ges + "&id=<?php echo $_GET["id"]?>",
                        
                        success: function(data){

                        }
    				});

    		});
    
    /*
    $(document).ready(function(){
				$('.deleteIcon').click(function(){
					if(confirm("Wollen Sie wirklich löschen?"))
					{
						var pid = $(this).attr('data-pid');
						//$('#row' + pid).hide();
						$.ajax({
							url:"http://127.0.0.1/htdocs/Medt/Ue8c/dbAccess-api.php",
							type:"POST",
							data:"todelete=" + pid,
							success:function(data){
								if(data){
									$('#message').html("Löschen erfolgreich!");
									$('#message').show();
									$('#row' + pid).remove();
								}
								else{
									$('#message').html("Löschen fehlgeschlagen!");
									$('#message').show();
									$('#row' + pid).remove();
								}
							},
							error:function(jqXHR, status){
								console.log(status);
							}
						});
					}
				});
			 });
*/
		</script>
</html>
