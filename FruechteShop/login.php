<html>
<head>
</head>
<body>
	<?php
	include 'db.php'; 

    //Admin
	$adm = $db ->prepare("select * from User where u_name=:bn and u_pw=:pwd and u_recht=2");
	$adm->execute(array(":bn" => $_POST['bn'],
						":pwd" => $_POST['pwd']));
	
    //User
    $usr = $db ->prepare("select * from User where u_name=:bn and u_pw=:pwd and u_recht=1");
	$usr->execute(array(":bn" => $_POST['bn'],
						":pwd" => $_POST['pwd']));

    
	if($adm -> rowCount() == 1)
	{
		session_start();
		$_SESSION['login'] = true;
		$_SESSION['user'] = $_POST['bn'];
		header("Location: mainpage_admin.php");
	}
	else if ($usr -> rowCount() == 1)
    {
		session_start();
		$_SESSION['login'] = true;
		$_SESSION['user'] = $_POST['bn'];
		header("Location: mainpage_client.php");
	} 
    else
		header("Location: index.php");
?>
</body>
</html>