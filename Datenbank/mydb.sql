-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 15. Jun 2016 um 08:28
-- Server Version: 5.6.21
-- PHP-Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `mydb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Artikel`
--

CREATE TABLE IF NOT EXISTS `Artikel` (
  `a_id` int(11) NOT NULL,
  `a_name` varchar(45) DEFAULT NULL,
  `a_beschreibung` varchar(45) DEFAULT NULL,
  `a_kosten` int(11) DEFAULT NULL,
  `a_anz` int(11) DEFAULT NULL,
  `l_id` int(11) DEFAULT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Artikel`
--

INSERT INTO `Artikel` (`a_id`, `a_name`, `a_beschreibung`, `a_kosten`, `a_anz`, `l_id`, `flag`) VALUES
(1, 'Apfel', 'Grüne Äpfel aus Spanien', 1, 210, 1, 0),
(2, 'Mandarienen', 'aus Spanien', 2, 432, 1, 1),
(3, 'Weintrauben', 'aus Spanien ohne Kerne', 2, 50, 1, 0),
(4, 'Melonen', 'groß, saftig', 3, 100, 1, 0),
(5, 'Tomate', 'aus Griechenland', 3, 90, 1, 0),
(7, 'Verpackung Bananen', 'für 10kg geeignet', 10, 150, 2, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Buchung`
--

CREATE TABLE IF NOT EXISTS `Buchung` (
  `a_id` int(11) DEFAULT NULL,
`b_id` int(11) NOT NULL,
  `b_datum` datetime DEFAULT NULL,
  `b_kosten` int(11) DEFAULT NULL,
  `b_anzahl` int(11) DEFAULT NULL,
  `k_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Buchung`
--

INSERT INTO `Buchung` (`a_id`, `b_id`, `b_datum`, `b_kosten`, `b_anzahl`, `k_name`) VALUES
(1, 2, '1998-02-24 12:23:21', 100, 50, 'Fruchtkonto'),
(1, 3, '2016-06-08 08:33:53', 10, 10, 'Fruchtkonto'),
(1, 4, '2016-06-08 08:34:28', 10, 10, 'Fruchtkonto'),
(5, 5, '2016-06-08 08:34:39', 30, 10, 'Fruchtkonto'),
(2, 6, '2016-06-08 08:35:02', 0, 10, 'Fruchtkonto'),
(3, 7, '2016-06-08 14:47:35', 20, 10, 'Fruchtkonto');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Konto`
--

CREATE TABLE IF NOT EXISTS `Konto` (
  `k_name` varchar(45) NOT NULL,
  `k_stand` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Konto`
--

INSERT INTO `Konto` (`k_name`, `k_stand`) VALUES
('FruchtKonto', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Lager`
--

CREATE TABLE IF NOT EXISTS `Lager` (
  `l_id` int(11) NOT NULL,
  `l_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Lager`
--

INSERT INTO `Lager` (`l_id`, `l_name`) VALUES
(1, 'Früchte'),
(2, 'Verpackungen');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `u_id` int(11) NOT NULL,
  `u_name` varchar(45) DEFAULT NULL,
  `u_pw` varchar(45) DEFAULT NULL,
  `u_recht` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `User`
--

INSERT INTO `User` (`u_id`, `u_name`, `u_pw`, `u_recht`) VALUES
(1, 'user', 'user', 1),
(2, 'admin', 'admin', 2);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `Artikel`
--
ALTER TABLE `Artikel`
 ADD PRIMARY KEY (`a_id`), ADD KEY `l_id_idx` (`l_id`);

--
-- Indizes für die Tabelle `Buchung`
--
ALTER TABLE `Buchung`
 ADD PRIMARY KEY (`b_id`), ADD KEY `a_id_idx` (`a_id`), ADD KEY `k_name_idx` (`k_name`);

--
-- Indizes für die Tabelle `Konto`
--
ALTER TABLE `Konto`
 ADD PRIMARY KEY (`k_name`);

--
-- Indizes für die Tabelle `Lager`
--
ALTER TABLE `Lager`
 ADD PRIMARY KEY (`l_id`);

--
-- Indizes für die Tabelle `User`
--
ALTER TABLE `User`
 ADD PRIMARY KEY (`u_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `Buchung`
--
ALTER TABLE `Buchung`
MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `Artikel`
--
ALTER TABLE `Artikel`
ADD CONSTRAINT `l_id` FOREIGN KEY (`l_id`) REFERENCES `Lager` (`l_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `Buchung`
--
ALTER TABLE `Buchung`
ADD CONSTRAINT `a_id` FOREIGN KEY (`a_id`) REFERENCES `Artikel` (`a_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `k_name` FOREIGN KEY (`k_name`) REFERENCES `Konto` (`k_name`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
